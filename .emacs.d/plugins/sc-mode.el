;;; sc-mode.el --- Major Mode for editing C source code -*- lexical-binding: t -*-

;; Copyright (C) 2021 rib

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;;; Commentary:
;;
;; Major Mode for editing C source code.

(defconst sc-mode-syntax-table
  (with-syntax-table (copy-syntax-table)
    ;; C/C++ style comments
	(modify-syntax-entry ?/ ". 124b")
	(modify-syntax-entry ?* ". 23")
	(modify-syntax-entry ?\n "> b")
	;; Preprocessor
	(modify-syntax-entry ?# ".")

	;; Chars are the same as strings
    (modify-syntax-entry ?' "\"")
    (syntax-table))
  "Syntax table for `sc-mode'.")

(eval-and-compile
  (defconst sc-keywords
	'("auto" "break" "case" "char" "const" "continue" "default" "do" "double"
      "else" "enum" "extern" "float" "for" "goto" "if" "int" "long" "register"
	  "i8" "i16" "i32" "i64" "u8" "u16" "u32" "u64" "f32" "f64"
      "return" "short" "signed" "sizeof" "static" "struct" "switch" "typedef"
      "union" "unsigned" "void" "volatile" "while" "alignas" "alignof" "and"
      "and_eq" "asm" "atomic_cancel" "atomic_commit" "atomic_noexcept" "bitand"
      "bitor" "bool" "catch" "char16_t" "char32_t" "char8_t" "class" "co_await"
      "co_return" "co_yield" "compl" "concept" "const_cast" "consteval" "constexpr"
      "constinit" "decltype" "delete" "dynamic_cast" "explicit" "export" "false" 
      "friend" "inline" "mutable" "namespace" "new" "noexcept" "not" "not_eq"
      "nullptr" "operator" "or" "or_eq" "private" "protected" "public" "reflexpr"
      "reinterpret_cast" "requires" "static_assert" "static_cast" "synchronized"
      "template" "this" "thread_local" "throw" "true" "try" "typeid" "typename"
      "using" "virtual" "wchar_t" "xor" "xor_eq")))

(defconst sc-highlights
  (list
   `("# *[a-zA-Z0-9_]+" . font-lock-preprocessor-face)
   `("#.*include \\(\\(<\\|\"\\).*\\(>\\|\"\\)\\)" . (1 font-lock-string-face))
   `(,(regexp-opt sc-keywords 'symbols) . font-lock-keyword-face)))

;;;###autoload
(define-derived-mode sc-mode prog-mode "sc"
  "Major Mode for editing C source code."
  (setq font-lock-defaults '(sc-highlights))
  (set-syntax-table sc-mode-syntax-table))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.c\\'" . sc-mode))

(provide 'sc-mode)

;;; sc-mode.el ends here
