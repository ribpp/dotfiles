(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(custom-set-variables
 '(custom-safe-themes
   '("03e26cd42c3225e6376d7808c946f7bed6382d795618a82c8f3838cd2097a9cc" default))
 '(display-line-numbers-type 'relative)
 '(global-display-line-numbers-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 '(default ((t (:family "Iosevka" :foundry "UKWN" :slant normal :weight semi-bold :height 120 :width normal)))))

;; Disable bloat
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)
(set-fringe-mode 0)

(setq inhibit-startup-screen t)
(setq visible-bell 1)
(setq ring-bell-function 'ignore) 

(setq make-backup-files nil)
(setq auto-save-default nil)

;; Hybrid line numbers
(global-display-line-numbers-mode t)
(setq display-line-numbers-type 'relative)

;; Themes
(load-theme 'gruber-darker)
;; (load-theme 'gruvbox)
;; (load-theme 'zenburn)

;; Minibuffer completion
(ido-mode 1)
(ido-everywhere 1)
(require 'ido-completing-read+)
(ido-ubiquitous-mode 1)

;; Transparent background
;;(set-frame-parameter (selected-frame) 'alpha '(90 . 75))
;;(add-to-list 'default-frame-alist '(alpha . (90 . 75)))

;; Default general / C style
(setq-default tab-width 4)
(setq c-default-style "k&r"
      c-basic-offset 4)

;; Run astyle command on region to format code
(defun astyle-buffer ()
  (interactive)
  (let ((saved-line-number (line-number-at-pos)))
    (shell-command-on-region
     (point-min)
     (point-max)
     "astyle --style=kr"
     nil
     t)
    (goto-line saved-line-number)))

;; Move current line up/down
(defun move-line-up ()
  (interactive)
  (transpose-lines 1)
  (previous-line 2))

(defun move-line-down ()
  (interactive)
  (next-line 1)
  (transpose-lines 1)
  (previous-line 1))

(global-set-key (kbd "C-M-<up>") 'move-line-up)
(global-set-key (kbd "C-M-<down>") 'move-line-down)

;; Recompile keybind
(global-set-key (kbd "M-m") 'recompile)

;; Custom sc-mode
(use-package sc-mode
  :load-path "~/.emacs.d/plugins/")

;; Custom sc-mode by file extension and hook
(add-to-list 'auto-mode-alist '("\\(\\.c\\|\\.h\\)\\'" . sc-mode))
(add-hook 'sc-mode-hook (lambda () (local-set-key (kbd "C-b f") #'astyle-buffer)))

;; Enable uppercase/downcase region keybinds
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; Subtitles (.srt) utilities
(use-package subed
  ;; Tell emacs where to find subed
  :load-path "~/.emacs.d/plugins/"
  :config
  ;; Disable automatic movement of point by default
  (add-hook 'subed-mode-hook 'subed-disable-sync-point-to-player)
  ;; Remember cursor position between sessions
  (add-hook 'subed-mode-hook 'save-place-local-mode)
  ;; Break lines automatically while typing
  (add-hook 'subed-mode-hook 'turn-on-auto-fill)
   ;; Break lines at 40 characters
  (add-hook 'subed-mode-hook (lambda () (setq-local fill-column 40))))

