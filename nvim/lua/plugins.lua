vim.cmd[[packadd packer.nvim]]

return require('packer').startup(function()
    --+-- PACKER ---+--
    use 'wbthomason/packer.nvim'

    --+--- THEMES ---+--
    use 'gruvbox-community/gruvbox'
    use 'jhlgns/naysayer88.vim'
    use 'tiagovla/tokyodark.nvim'

    --+--- STATUS LINE ---+--
    use 'itchyny/lightline.vim'

    --+--- LPS ---+--
    use 'neovim/nvim-lspconfig'
    use 'nvim-lua/completion-nvim'

    --+--- FILES ---+--
    use {
        'nvim-telescope/telescope.nvim',
        requires = {{ 'nvim-lua/plenary.nvim' }}
    }

end)
